﻿using Aplikacja.Models;
using Aplikacja.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Aplikacja.ViewModels
{
    public class ResultsViewModel : INotifyPropertyChanged
    {
        private readonly ApiServices _apiServices = new ApiServices();
        private List<Result> _results;

        public ResultsViewModel()
        {
            ExecuteLoadItems();
        }
        public async void ExecuteLoadItems()
        {
            await Task.Run(async () => { Results = await _apiServices.GetPeopleAsync(); });

        }

        //public string AccessToken { get; set; }
        public List<Result> Results
        {
            get { return _results; }
            set
            {
                _results = value;
                OnPropertyChanged();
            }
        }


   

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}