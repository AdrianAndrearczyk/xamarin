﻿using System;
using System.Windows.Input;

using Xamarin.Forms;

namespace Aplikacja
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "About";

            OpenWebCommand = new Command(() => Device.OpenUri(new Uri("https://swapi.co/")));
        }

        public ICommand OpenWebCommand { get; }
    }
}