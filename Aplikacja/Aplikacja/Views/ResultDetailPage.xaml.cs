﻿using Aplikacja.Models;
using Aplikacja.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aplikacja.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultDetailPage : ContentPage
    {
        public ResultDetailPage(Result result)
        {
            InitializeComponent();

            var editIdeaViewModel = new ResultDetailViewModel();
            //var editIdeaViewModel = BindingContext as EditIdeaViewModel;

            editIdeaViewModel.Result = result;

            BindingContext = editIdeaViewModel;
        }
    }
}