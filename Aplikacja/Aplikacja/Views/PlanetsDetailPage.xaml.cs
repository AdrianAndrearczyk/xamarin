﻿using Aplikacja.Models;
using Aplikacja.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aplikacja.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PlanetsDetailPage : ContentPage
    {
        public PlanetsDetailPage(Result result)
        {
            InitializeComponent();

            var planetDetailViewModel = new PlanetDetailViewModel();
            //var editIdeaViewModel = BindingContext as EditIdeaViewModel;

            planetDetailViewModel.Result = result;

            BindingContext = planetDetailViewModel;
        }
    }
}