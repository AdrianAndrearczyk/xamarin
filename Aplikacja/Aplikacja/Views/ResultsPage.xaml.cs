﻿using Aplikacja.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Aplikacja.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultsPage : ContentPage
    {
        public ResultsPage()
        {
            InitializeComponent();            
        }

        private async void ResultsListView_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var result = e.Item as Result;
            await Navigation.PushAsync(new ResultDetailPage(result));
        }
    }
}