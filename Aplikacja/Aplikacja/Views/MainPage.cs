﻿using Aplikacja.Views;
using System;

using Xamarin.Forms;

namespace Aplikacja
{
    public class MainPage : TabbedPage
    {
        public MainPage()
        {
            Page aboutPage, resultsPage, planetsPage = null;

            switch (Device.RuntimePlatform)
            {
                default:
                    aboutPage = new AboutPage()
                    {
                        Title = "About"
                    };

                    resultsPage = new ResultsPage()
                    {
                        Title = "People"
                    };

                    planetsPage = new PlanetsPage()
                    {
                        Title= "Planets"
                    };
                    break;
            }

            Children.Add(aboutPage);
            Children.Add(resultsPage);
            Children.Add(planetsPage);

            Title = Children[0].Title;
        }

        protected override void OnCurrentPageChanged()
        {
            base.OnCurrentPageChanged();
            Title = CurrentPage?.Title ?? string.Empty;
        }
    }
}
