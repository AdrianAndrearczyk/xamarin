﻿using Aplikacja.Models;
using Microsoft.VisualBasic;
using ModernHttpClient;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Aplikacja.Services
{
    internal class ApiServices
    {
  
        public async Task<List<Result>> GetPeopleAsync()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());

            var json = await client.GetStringAsync(Constants.BaseApiAddress + "people");

            var people = JsonConvert.DeserializeObject<RootObject>(json).results;
            return people;
        }

        public async Task<List<Result>> GetPlanetsAsync()
        {
            HttpClient client = new HttpClient(new NativeMessageHandler());

            var json = await client.GetStringAsync(Constants.BaseApiAddress + "planets");

            var planets = JsonConvert.DeserializeObject<RootObject>(json).results;
            return planets;
        }

    }
}