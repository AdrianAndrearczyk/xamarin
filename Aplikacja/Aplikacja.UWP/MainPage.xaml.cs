﻿namespace Aplikacja.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            this.InitializeComponent();
            LoadApplication(new Aplikacja.App());
        }
    }
}